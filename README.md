# check html change
A small python script detects changes of a website.
Designed to be fast and simple.

Changes are detected by:

* CRC32 checksum of the online html vs downloaded one from last run
* Keyword presents in the pharsed online html

# Requirement
## Recommended (not required):
 * `notify-send` provided by Linux Distribution 
 * Desktop Environment with notification support (See also at [Archwiki](https://wiki.archlinux.org/index.php/Desktop_notifications))

# Installation
1. Copy `config.sample.py` to `config.py`
2. Modify `config.py`

# Run
## Scheduled Task
* Systemd timer is supported. (See [Archwiki](https://wiki.archlinux.org/index.php/Systemd/Timers)) 

* Cron is **not** supported.
	* Desktop Notification not working. ( **HELP NEEDED** )
	* Everything else is working, e.g. command line output, change detection.
	* However, it is pointless since you would not be notified if the website changed.
 
# License
See [LICENSE](LICENSE)
	
	Copyright (C) 2020  Adam S
	GPLv3