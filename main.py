#!/usr/bin/env python3
import zlib, pickle, os, subprocess, urllib.request, config
from html.parser import HTMLParser
from pathlib import Path

match_count = 0
match_pattern = config.match_pattern
class MyHTMLParser(HTMLParser):
    #def handle_starttag(self, tag, attrs):
        #print("Encountered a start tag:", tag)

    #def handle_endtag(self, tag):
        #print("Encountered an end tag :", tag)

    def handle_data(self, data):
        global match_count
        #print("Encountered some data  :", data)
        if (data in match_pattern):
            #print("FOUND")
            match_count+= 1
            #print(match_count)

parser = MyHTMLParser()

os.chdir( Path(__file__).resolve().parent )

if os.path.isfile("web_download"):
    os.rename("web_download", "web_download.old")

data_pickle = None
if os.path.isfile("data.pickle"):
    with open('data.pickle', 'rb') as f:
        # The protocol version used is detected automatically, so we do not
        # have to specify it.
        data_pickle = pickle.load(f)

url = config.url

req = urllib.request.Request(url, headers={'User-Agent' : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"})
response = urllib.request.urlopen(req)

data_raw = response.read()
data = data_raw.decode('utf-8')

parser.feed(data)

data_crc32 = zlib.crc32(data.encode('utf-8')[28500:31000])
#data_crc32 = None

#print(data_crc32)

msg = ""
pattern_changed = None

if data_crc32 == data_pickle:
    msg += "crc32 same \n"

else:
    msg += "crc32 CHANGED \n"

if len(match_pattern) == match_count:
    msg += "match_count checked: " + str(match_count) + "/" + str(len(match_pattern))
    pattern_changed = False

else:
    msg += "match_count FAILED: " + str(match_count) + "/" + str(len(match_pattern))
    pattern_changed = True

with open('data.pickle', 'bw+') as f:
    pickle.dump(data_crc32, f, pickle.HIGHEST_PROTOCOL)

with open('web_download', 'w+') as f:
    f.write(data)
    
print(msg)

if pattern_changed:
    arg = "env DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus notify-send -u critical \"🟨🟨URGET!!! Sumeasy Website CHANGED🟨🟨\" " 
    arg += '\'<a href="' 
    arg += url
    arg += '">Website CHANGED</a>\n'
    arg += msg 
    arg += '\''
    subp_run = subprocess.run([arg], shell=True, check=True)
